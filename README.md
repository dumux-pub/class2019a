This contains all required files to reproduce the results of [__Experimental and Simulation Study on Validating a Numerical Model for CO2 Density-Driven Dissolution in Water__](https://doi.org/10.3390/w12030738)

```
@article{Class2020,
  doi = {10.3390/w12030738},
  url = {https://doi.org/10.3390/w12030738},
  year = {2020},
  month = mar,
  publisher = {{MDPI} {AG}},
  volume = {12},
  number = {3},
  pages = {738},
  author = {Holger Class and Kilian Weishaupt and Oliver Tr\"{o}tschler},
  title = {Experimental and Simulation Study on Validating a Numerical Model for {CO}2 Density-Driven Dissolution in Water},
  journal = {Water}
}
```
Installation with Docker
========================
To install this module with Docker is t downlaod the docker image
````
wget "https://git.iws.uni-stuttgart.de/dumux-pub/class2019a/-/raw/master/docker/pubtable_class2019a"
````
Open the docker image
````
bash pubtable_class2019a open
````

Installation
============

The easiest way to install this module and its dependencies is to create a new
directory

```
mkdir New_Folder && cd New_folder
```
and download the install script

[installClass2019a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/class2019a/raw/master/installClass2019a.sh)

to that folder. Run the script with

```
chmod u+x installClass2019a.sh
./installClass2019a.sh
```

After the script has finished sucessfully, you may build the program executables

```
cd class2019a/build-cmake/appl/convectivemixing
make test_densitydrivenflow_3d
make test_densitydrivenflow_2d
```

and run them, e.g., with 
```
./test_densitydrivenflow_3d test_densitydrivenflow_3d.input
```

