#!/bin/sh

# dune-common
git clone https://gitlab.dune-project.org/core/dune-common.git
cd dune-common
git checkout releases/2.7
cd ..

# dune-geometry
git clone https://gitlab.dune-project.org/core/dune-geometry.git
cd dune-geometry
git checkout releases/2.7
cd ..

# dune-grid
git clone https://gitlab.dune-project.org/core/dune-grid.git
cd dune-grid
git checkout releases/2.7
cd ..

# dune-localfunctions
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
cd dune-localfunctions
git checkout releases/2.7
cd ..

# dune-istl
git clone https://gitlab.dune-project.org/core/dune-istl.git
cd dune-istl
git checkout releases/2.7
cd ..

# dumux
# master # c77e4d15d465a34f1f2569708d7d4df90c3c7e65 # 2020-01-29 13:43:07 # Timo Koch
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
cd dumux
git checkout master
git reset --hard c77e4d15d465a34f1f2569708d7d4df90c3c7e65
cd ..

# the pub-module containing the code for the example
git clone https://git.iws.uni-stuttgart.de/dumux-pub/class2019a

# configure project
./dune-common/bin/dunecontrol --opts=dumux/cmake.opts all
